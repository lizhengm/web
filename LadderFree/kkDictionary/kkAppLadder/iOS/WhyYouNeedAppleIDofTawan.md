##### [关于Apple ID](https://gitlab.com/kk404/web/-/blob/master/LadderFree/kkDictionary/kkAppLadder/iOS/AppleID.md)
### 为何需要一个台湾区 Apple ID?
#### 创建、使用一个台湾区 Apple ID 是因为
- <strong>个人隐私和信息安全</strong>
  - 台湾政府至今没有要求苹果公司将苹果云iCloud中保存的用户个人信息向它自己或向由它指定的某一间台湾公司开放

- <strong>获取稀缺商品</strong>
  - 获取您当前Apple ID所在苹果软件商店App Store中所缺少的应用软件，如云梯类工具

- <strong>繁体中文</strong>
  - 对讲汉语写中文的金刚之友而言，与美洲区、欧洲区的苹果App Store相比，台湾区苹果App Store具有语言文字的独到优势

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)
- [关于Apple ID](/LadderFree/kkDictionary/kkAppLadder/iOS/AppleIDList.md)
