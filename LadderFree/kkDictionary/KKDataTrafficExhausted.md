###### 玩转金刚梯>金刚字典>

### 流量提前耗尽
- [ 金刚 ](/LadderFree/kkDictionary/Atozitpro.md)卖的[ 流量包 ](/LadderFree/kkDictionary/KKDataTrafficPackage.md)是附带[ 有效期 ](/LadderFree/kkDictionary/KKDataTrafficPackageExpiretion.md)的
- 所谓<Strong> 流量提前耗尽 </Strong >是指：[ 流量包 ](/LadderFree/kkDictionary/KKDataTrafficPackage.md)尚未过期，但包内[ 流量 ](/LadderFree/kkDictionary/KKDataTraffic.md)提前用完的情景


#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

