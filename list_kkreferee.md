###### 金刚梯>金刚帮助>金刚伙伴>
### 金刚推荐人类

- [什么叫金刚推荐人？](/kkreferee.md)
- [金刚推荐人能得到啥回报？](/rewardforkkreferee.md)
- [什么样的人才有资格作金刚推荐人？](/qualificationofkkreferee.md)
- [金刚推荐人如何推荐金刚给新人？](/workingmethodsofkkreferee.md)



#### 推荐阅读
- [金刚梯](/dlb.md)
- [金刚帮助](/list_helpkkvpn.md)
- [金刚1.0金刚号梯](/list_helpkkvpn1.0.md)
- [金刚2.0app梯](/list_helpkkvpn2.0.md)
- [金刚伙伴](/list_kkpartner.md)
