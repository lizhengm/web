#### 金刚梯>金刚帮助>金刚2.0app梯>
### 安装说明
- 适用于 苹果平板iPad

- 特色
  - 一键翻墙
  - 安全高速 
  - 多条线路 
  - 免费公测 
  - HTTPS/SSL VPN

- 《安装说明》
  - 从略


#### 推荐阅读
- [金刚梯](/dlb.md)
- [金刚帮助](/list_helpkkvpn.md)
- [金刚公司类](/list_kk.md)
- [金刚1.0金刚号梯](/list_helpkkvpn1.0.md)
- [金刚2.0app梯](/list_helpkkvpn2.0.md)
- [金刚2.0app梯产品获取与安装](/list_kkproducts2.0.md)
